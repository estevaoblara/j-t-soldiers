BALADEIROS
Objetivo: Facilitar pessoas de diversas faixas etárias a achar uma festa a qual seu perfil se encaixa em qualquer lugar que estiver.

Como funciona: 
1.	O usuário acessa o site do Baladeiros;

2.	Na página inicial tem as baladas mais visitadas e com maior votação do público;

3.	O usuário tem a opção de cadastrar-se ou realizar o Login;

4.	Se o usuário estiver logado terá a opção de  pesquisar baladas de acordo com seu gênero, poderá ver as baladas mais votadas, as festas mais próximas, que música tocará e o valor da entrada.

5.	Inicialmente e equipe do Baladeiros realizará o cadastro das baladas e festas, mas futuramente os próprios donos de baladas poderão adicionar suas festas no Baladeiros;




Renda:

1.	Google Adsense;
2.	Divulgação em lucar reservado na página inicial;
3.	Views;
4.	Porcentagem na venda de ingressos ;



Despesas:

1.	Hospedagem;
2.	Luz;
3.	Alugel;
4.	Pró-labore;



Parcerias chaves:

1.	Baladas;
2.	Empresas de TI;


Segmentos de Mercado:
1.	Eventos em geral;



Proposta de Valor:
1.	Entretenimento;
