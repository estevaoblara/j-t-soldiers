<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
 
  <meta charset="utf-8" />
  <title>Login Baladeiros</title>
  <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
  
  <script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
  <script src="semantic/semantic.min.js"></script>

  <script src="/semantic/semantic.min.js"></script>
  <script src="/semantic/semantic.js"></script>

  <style type="text/css">
    body {
      background-color: #000000;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
    img {
      display: inline-block;
    }
  </style>

</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">

    <h2 class="ui inverted blue image header">
      <div class="content">
       <br/>
        <img src="images/v2.png">
        Entre em sua conta
      </div>
    </h2>

    <form class="ui large form" name="loginForm" action="conn.php" method="POST">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="Name" placeholder="Endereço de e-mail">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Senha">
          </div>
        </div>
        <input type="submit" class="ui fluid large inverted blue submit button" value="Login">
      </div>

      <div class="ui error message"></div>

    </form>

    <div class="ui message">
      Usuário novo? <a href="#">Registre-se</a>
    </div>
  </div>
</div>

</body>

</html>
