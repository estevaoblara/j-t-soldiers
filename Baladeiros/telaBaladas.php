<?php

/*Variáveis Forms */
$user = $_COOKIE["Name"];
$pas = $_COOKIE["Pass"];

?>

    <!DOCTYPE html>
    <html lang="en">
    <head>

        <link rel="stylesheet" type="text/css" href="../dist/components/reset.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/site.css">

        <link rel="stylesheet" type="text/css" href="../dist/components/container.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/grid.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/header.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/image.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/menu.css">

        <link rel="stylesheet" type="text/css" href="../dist/components/divider.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/dropdown.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/segment.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/button.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/list.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/icon.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/sidebar.css">
        <link rel="stylesheet" type="text/css" href="../dist/components/transition.css">

        <script src="assets/library/jquery.min.js"></script>
        <script src="../dist/components/visibility.js"></script>
        <script src="../dist/components/sidebar.js"></script>
        <script src="../dist/components/transition.js"></script>
        <script>
            $(document)
                .ready(function() {

                    // fix menu when passed
                    $('.masthead')
                        .visibility({
                            once: false,
                            onBottomPassed: function() {
                                $('.fixed.menu').transition('fade in');
                            },
                            onBottomPassedReverse: function() {
                                $('.fixed.menu').transition('fade out');
                            }
                        })
                    ;

                    // create sidebar and attach to menu open
                    $('.ui.sidebar')
                        .sidebar('attach events', '.toc.item')
                    ;

                })
            ;
        </script>

        <!-- Standard Meta -->
        <style>
            #map {
                height: 400px;
                width: 100%;
            }
        </style>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">

        <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
        <script src="semantic/semantic.min.js"></script>

        <script src="/semantic/semantic.min.js"></script>
        <script src="/semantic/semantic.js"></script>

        <style type="text/css">
            body {
                background-color: #000000;
                font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;

            }
            body > .grid {
                height: 100%;
            }
            .image {
                margin-top: -100px;
            }
            .column {
                max-width: 450px;
            }
            .colorm {
                color: deepskyblue;
            }
            .comment {
                color: white !important;
            }
            .color {
                color: white;
            }

        </style>

        <title>Baladeiros</title>

    </head>

    <body>
    <!-- Page Contents -->
    <div class="pusher">
        <div class="ui inverted vertical masthead center aligned segment">

            <div class="ui container">

                <div class="ui large secondary inverted pointing menu">

                    <a href="#" class="header item">
                        <img class="logo" src="logo_globo.png" height="40px" width="40px">
                    </a>

                    <a class=" item" href="index.php">Home</a>
                    <a class="active item" href="telaBaladas.php">Baladas</a>

                    <div class="right item">


                        <?php
                        require 'conexao.php';

                        $sql = "SELECT User FROM user WHERE User = '$user' AND Pass = '$pas'";

                        $result = $conn->query($sql);


                        if ($result->num_rows > 0) {

                                echo "<a class=\"ui inverted green button\">".$_COOKIE['Name']."</a>";

                        } else {

                            echo "<a class=\"ui inverted green button\" href='telaLogin.html'>Logar</a>";
                        }

                        echo "<a class=\"ui inverted blue button\" href='sair.php'>Sair</a>";


                        $conn->close();
                        ?>

                    </div>
                </div>
        <br/><br/><br/><br/>

        <?php
        require 'conexao_balada.php';



        $sql = "SELECT * FROM festa";

        $result = $conn->query($sql);


        ?>
        <h2>Baladas</h2>
<table class="ui celled padded table">
    <thead>
    <tr>
        <th class="single line">Nota no Ranking</th>
        <th>Nome</th>
        <th>Faixa etária</th>
        <th>Descrição</th>
        <th>Data Inicial</th>
        <th>Data Final</th>

    </tr></thead>
    <tbody>
    <?php
    if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {

    echo "
    <tr>
        
                <td class=\"ui center aligned header\">".$row["Avaliacao"]."/5</td>

                <td class=\"single line\">".$row["Nome"]."</td>
        
        <td>
            <div class=\"ui center aligned header\">".$row["FaixaEtaria"]."</div>
        </td>
        <td class=\"right aligned\">
".$row["DescricaoFesta"]." <br>
            
        </td>
        <td class=\"single line\">
".$row["DataHoraInicio"]."
</td>
<td>".$row["DataHoraFim"]."</td>
    </tr> ";
 }
    }
    ?>
    </tbody>
</table>
</body>
</html>



 <?php



    $conn->close();
 ?>