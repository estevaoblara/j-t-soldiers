<?php

/*Variáveis Forms */
$user = $_COOKIE["Name"];
$pas = $_COOKIE["Pass"];

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">

    <script src="assets/library/jquery.min.js"></script>
    <script src="../dist/components/visibility.js"></script>
    <script src="../dist/components/sidebar.js"></script>
    <script src="../dist/components/transition.js"></script>
    <script>
        $(document)
            .ready(function() {

                // fix menu when passed
                $('.masthead')
                    .visibility({
                        once: false,
                        onBottomPassed: function() {
                            $('.fixed.menu').transition('fade in');
                        },
                        onBottomPassedReverse: function() {
                            $('.fixed.menu').transition('fade out');
                        }
                    })
                ;

                // create sidebar and attach to menu open
                $('.ui.sidebar')
                    .sidebar('attach events', '.toc.item')
                ;

            })
        ;
    </script>

    <!-- Standard Meta -->
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="semantic/semantic.min.js"></script>

    <script src="/semantic/semantic.min.js"></script>
    <script src="/semantic/semantic.js"></script>


    <style type="text/css">
        body {
            background-color: #000000;
            font-family: 'Ubuntu', sans-serif;

        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
        .colorm {
            color: deepskyblue;
        }
        .color {
            color: white;
        }

        .botao {
            position: fixed;
            align-content: ;
            top: 90%;

        }

        .cortop {
            color: #FF7A00;

        }
        .lista {
            height: 50%;
            color: white;
        }
    </style>

    <title>Baladeiros</title>


</head>

<body>



<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu">
    <a class="active item">Home</a>
    <a class="item">Baladas</a>
    <?php
    require 'conexao.php';

    $sql = "SELECT User FROM user WHERE User = '$user' AND Pass = '$pas'";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {

        echo "<a class=\"ui inverted green button\">".$_COOKIE['Name']."</a>";

    } else {

        echo "<a class=\"ui inverted green button\" href='telaLogin.php'>Logar</a>";
    }

    echo "<a class=\"ui inverted blue button\" href='sair.php'>Sair</a>";


    $conn->close();
    ?>

</div>


<!-- Page Contents -->
<div class="pusher">
    <div class="ui inverted vertical masthead center aligned segment">

        <div class="ui container">

            <div class="ui large secondary inverted pointing menu">
                <a href="#" class="header item">
                    <img class="logo" src="logo_globo.png" height="40px" width="40px">
                </a>

                <a class="active item" href="index.php">Home</a>
                <a class="item" href="telaBaladas.php">Baladas</a>
                <div class="right item">
                    <?php
                    require 'conexao.php';

                    $sql = "SELECT User FROM user WHERE User = '$user' AND Pass = '$pas'";

                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {

                        echo "<a class=\"ui inverted green button\">".$_COOKIE['Name']."</a>";

                    } else {

                        echo "<a class=\"ui inverted green button\" href='telaLogin.php'>Logar</a>";
                    }

                    echo "<a class=\"ui inverted blue button\" href='sair.php'>Sair</a>";


                    $conn->close();
                    ?>

                </div>
            </div>

        </div>

        <img class="logo" src="background_logo.png" width="100%" height="50%">

    </div>
    <div class="color">
        <div class="ui vertical stripe segment">
            <div class="ui middle aligned stackable grid container">
                <div class="row">
                    <div class="eight wide column">
                        <h2 class="ui inverted black header">Sobre o Baladeiros</h2>
                        <p>Baladeiros é o melhor lugar do universo para encontrar a sua festa ideal e se divertir a qualquer momento!<br/>
                            <br/>Além de te dar a oportunidade de encontrar os lugares mais divertidos e frequentados do mundo das partyes, mostra as festas que estão prestes a acontecer perto de você!<br/>
                            <br/>Quer divulgar uma festa? Não tem problema! O nosso sitema pode cadastrar quantas festas quiser a qualquer momento.<br/>
                            <br/>Nossos pacotes de divulgação te colocam na #TOPBALADAS a lista das festas mais bem cotadas do mundo , lhe dando um grande UP na divugação da sua grande festa.<br/>
                            <br/>Encontre aquela festa na sua viagem a um local desconhecido.<br/>
                            <br/>Ache a festa que represente a sua vida.</p>

                    </div>
                    <div class="six wide right floated column">
                        <img src="assets/images/wireframe/white-image.png" class="ui large bordered rounded image">
                    </div>
                </div>
                <div class="row">
                    <div class="center aligned column">
                        <a class="ui inverted yellow huge button">Conferir baladas</a>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        <div class="ui vertical stripe segment">

            <h2 class="ui horizontal header divider">
                <a class="cortop">#TOPBALADAS</a>
            </h2>
        </div>
    </div>
    <div class="ui middle aligned center ">
        <div class="lista" style="margin-left:25%;width: 50%" >
            <?php
            require 'conexao_balada.php';



            $sql = "SELECT  * FROM festa  ORDER BY Avaliacao DESC LIMIT 5 ";

            $result = $conn->query($sql);


            ?>
            <div class="ui inverted segment">
                <div class="ui inverted massive ordened text-center divided list">
                    <?php
                    if ($result->num_rows > 0) {

                        while($row = $result->fetch_assoc()) {

                            echo "
    
        <div class=\"item\">
                        <div class=\"content\">
                            <div class=\"header\"><h2>".$row["Nome"]."</h2></div>
                            ".$row["DescricaoFesta"]."
                        </div>
                    </div>
                ";
                        }
                    }
                    ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<h3 class="colorm">Mapa Baladeiro</h3>
<div id="map"></div>
<script>
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: new google.maps.LatLng(-33.91722, 151.23064),
            mapTypeId: 'roadmap'
        });


        var icons = {
            1: {
                icon: 'paradagay.png'
            },
            2: {
                icon: 'normal.png'
            },
            3: {
                icon: 'chopp.png'
            }
        };

        var features = [
            {
                position: new google.maps.LatLng(-33.91721, 151.22630),
                type: '1'
            }, {
                position: new google.maps.LatLng(-33.91539, 151.22820),
                type: '1'
            }, {
                position: new google.maps.LatLng(-33.91747, 151.22912),
                type: '2'
            }, {
                position: new google.maps.LatLng(-33.91818154739766, 151.2346203981781),
                type: '3'
            }, {
                position: new google.maps.LatLng(-33.91727341958453, 151.23348314155578),
                type: '2'
            }
        ];

        // Create markers.
        features.forEach(function(feature) {
            var marker = new google.maps.Marker({
                position: feature.position,
                icon: icons[feature.type].icon,
                map: map
            });
        });

    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbbrdennF7B2pyE38xZtTiYBFuvLDCa_I&callback=initMap"> </script>

</div>

<a href="#top" class="botao"><img src="http://www.diadema.sp.gov.br/images/imagens_site/seta_cima-scroll.png"/></a>



</body>



</html>