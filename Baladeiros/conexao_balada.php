<?php
/**
 * Created by PhpStorm.
 * User: estevao.lara
 * Date: 11/11/2017
 * Time: 05:14
 */

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "festas";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
