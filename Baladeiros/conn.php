<?php

/*Variáveis Forms */
$user = $_POST['Name'];
$pas = $_POST['password'];

/*Cookies*/
$cookie_name = $user;
$cookie_pass = $pas;

setcookie("Pass", $cookie_pass);
setcookie("Name", $cookie_name);


session_start()

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" type="text/css" href="../dist/components/reset.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/site.css">

    <link rel="stylesheet" type="text/css" href="../dist/components/container.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/grid.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/header.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/image.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/menu.css">

    <link rel="stylesheet" type="text/css" href="../dist/components/divider.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/dropdown.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/segment.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/button.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/list.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/icon.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/sidebar.css">
    <link rel="stylesheet" type="text/css" href="../dist/components/transition.css">

    <script src="assets/library/jquery.min.js"></script>
    <script src="../dist/components/visibility.js"></script>
    <script src="../dist/components/sidebar.js"></script>
    <script src="../dist/components/transition.js"></script>
    <script>
        $(document)
            .ready(function() {

                // fix menu when passed
                $('.masthead')
                    .visibility({
                        once: false,
                        onBottomPassed: function() {
                            $('.fixed.menu').transition('fade in');
                        },
                        onBottomPassedReverse: function() {
                            $('.fixed.menu').transition('fade out');
                        }
                    })
                ;

                // create sidebar and attach to menu open
                $('.ui.sidebar')
                    .sidebar('attach events', '.toc.item')
                ;

            })
        ;
    </script>

    <!-- Standard Meta -->
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="semantic/semantic.min.js"></script>

    <script src="/semantic/semantic.min.js"></script>
    <script src="/semantic/semantic.js"></script>

    <style type="text/css">
        body {
            background-color: #000000;
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;

        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
        .colorm {
            color: deepskyblue;
        }
        .comment {
            color: white !important;
        }
        .color {
            color: white;
        }

    </style>

    <title>Baladeiros</title>

</head>

<body>
<!-- Page Contents -->
<div class="pusher">
    <div class="ui inverted vertical masthead center aligned segment">

        <div class="ui container">

            <div class="ui large secondary inverted pointing menu">

                <a href="#" class="header item">
                    <img class="logo" src="logo_globo.png" height="40px" width="40px">
                </a>

                <a class="active item" href="index.php">Home</a>
                <a class="item" href="telaBaladas.php">Baladas</a>
                <a class="item">Contatos</a>
                <div class="right item">


                    <?php
                    require 'conexao.php';

                    $sql = "SELECT User FROM user WHERE User = '$user' AND Pass = '$pas'";

                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {

                            echo "<a class=\"ui inverted green button\">".$_COOKIE['Name']."</a>";

                    } else {

                        echo "<a class=\"ui inverted green button\" href='telaLogin.php'>Logar</a>";
                    }

                    echo "<a class=\"ui inverted blue button\" href='sair.php'>Sair</a>";


                    $conn->close();

                    ?>

                </div>
            </div>

        </div>
        <img class="logo" src="background_logo.png" width="100%" height="50%">

    </div>
    <div class="color">
        <div class="ui vertical stripe segment">
            <div class="ui middle aligned stackable grid container">
                <div class="row">
                    <div class="eight wide column">
                        <h3 class="ui inverted black header">Sobre o Baladeiros</h3>
                        <p>Texto de decrição do projeto Texto de decrição do projeto Texto de decrição do projeto Texto de decrição do projeto Texto de decrição do projeto </p>

                    </div>
                    <div class="six wide right floated column">
                        <img src="assets/images/wireframe/white-image.png" class="ui large bordered rounded image">
                    </div>
                </div>
                <div class="row">
                    <div class="center aligned column">
                        <a class="ui huge button">Conferir baladas</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="ui vertical stripe segment">

            <h4 class="ui horizontal header divider">
                <a href="#">Top Baladas da Semana</a>
            </h4>
        </div>
    </div>
</div>

<div class="ui inverted vertical footer segment">
    <div class="ui container">
        <div class="ui stackable inverted divided equal height stackable grid">
            <a href="https://www.blueticket.com.br/20402/Vintage-Culture-Grand-Celebration-Sabado/?obj=listagem-tipo">1º Vintage Culture - Grand Celebration</a>
            <a href=""></a>
        </div>

    </div>
</div>
</div>
</div>
<h3 class="colorm">Google Maps</h3>
<div id="map"></div>
<script>
    function initMap() {
        var uluru = {lat: -25.363, lng: 310.000};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbbrdennF7B2pyE38xZtTiYBFuvLDCa_I&callback=initMap"> </script>

</div>

</body>
</html>